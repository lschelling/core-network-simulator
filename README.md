# README #

This is a vagrant file to setup the CORE network emulator inside a VirtualBox VM.

The image contains a docker service to run services and applications inside the network simulator.

### Pre-requisites ###

* Vagrant >1.7 
* VirtualBox
* Install vagrant box ubuntu-1404-desktop
    $ vagrant box add --insecure https://vagrantcloud.com/janihur/ubuntu-1404-desktop --provider virtualbox

### Setup ###

    $ git clone https://lschelling@bitbucket.org/lschelling/core-network-simulator.git
    $ cd core-network-simulator
    $ vagrant up


The whole setup process is automated as much as possible. During the *vagrant up* process the user will be asked for interface of the host machine which should be briged to the VM. This is typically the interface connected to the 'real' device under test.


```
==> default: which network to bridge to.
==> default: Available bridged network interfaces:
1) wlan0
2) eth0

```

## What is in the box?##

### CORE Network Emulator ###
The CORE user interface can be started from the menu *System Tools*.
The core daemon is already running.

### Configuration files ###
After the VM is up and running the current directory (the directory from where you called vagrant up) is mapped to the /vagrant folder inside the VM. Thus all the files from this repository are visible in the /vagrant folder.

The folder CORE_Scenarios contains a set of sample configuration files.

### VLAN interfaces / driver ###
The network simulator VM comes with enabled vlan support.

The VM has preconfigured single and double tagged vlan interfaces.

* eth0 --> untagged interface
* eth0.1000 --> single tagged vlan interface with vlan id 1000.
* eth0.1000.2000 --> double tagged vlans interface: 1000 (outer vlan) / 2000 (inner vlan).

These interfaces can be used from inside the network emulator. More vlan interfaces can be added using the vconfig command.

### Networking Tools ###

* Firefox
* Wireshark
* tshark
* traceroute
* iperf
* 802.1ag tools (ethping, ethtrace, dot1agd, dot1ag_ccm)

and more

### Java Runtime ###
Used to run Java applications such as the ULAF+ LCT+ or the MetroIntegrator.

### Docker RADIUS ###
This repository contains a Dockerfile for a test RADIUS server with preconfigured test users. The documentation of the RADIUS server is described in the docker/radius/README.md file.

To enable the RADIUS server add the line *docker run -d --net=host user/radius*  as the last line in the Docker node configuration file.
(check out the sample configuration file CORE_Scenarios/core-radius.imn)
