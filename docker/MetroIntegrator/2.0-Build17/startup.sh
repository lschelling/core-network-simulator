#!/bin/bash

#important: if you edit this file, make sure you save it with unix line endings (i.e. <LF>, not <CR><LF>)

##build base container:
#docker build -t albistechnologies/metrointegrator:2.0Build17_Base .
#
##run for installation:
#docker run -it --net host albistechnologies/metrointegrator:2.0Build17_Base
#
##inside container:
#./install.sh /opt/ems metrointegrator-2.0-Build17-Linux.tar.gz
##customize installation as needed
#
##after installation, wait for metrointegrator to start, check log (look for "Exiting thread: ModuleManagerStartupThread"):
#tail -f /opt/ems/metrointegrator/application/logs/metrointegrator.log
#service metrointegrator stop
#service ec_watchdog stop
#service ec_postgres stop
#exit
#
##look up container name (latest container should be at the top):
#docker ps -a
#docker commit -c "entrypoint /root/MetroIntegrator/startup.sh" $ContainerName albistechnologies/metrointegrator:2.0Build17_Installed
#docker rm $ContainerName
#
#run example:
#docker run -it --net host --rm=true albistechnologies/metrointegrator:2.0Build17_Installed

service ec_postgres start
service metrointegrator start
service ec_watchdog start
tail -f /opt/ems/metrointegrator/application/logs/metrointegrator.log