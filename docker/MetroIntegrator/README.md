   
## MetroIntegrator Build ##


The MetroIntegrator image is somewhat tricky to set up due to its interactive setup process. For now, this process has not been automated, but that would certainly be a possibility in the future.
The setup process required the build of a base image which contains all dependencies and installation files. This image then needs to be executed once to perform manual installation and initialization of the application. The entire process has already been done for MetroIntegrator 2.0 Build 15 & 17. However, it will have to be done again for each new release.
Build directory in VM: /home/ulaf/Documents/docker-metroIntegrator/MI_2.0_Build_17_Linux
Tag (Base image): albistechnologies/metrointegrator:2.0Build17_Base
Tag (Installed image): albistechnologies/metrointegrator:2.0Build17_Installed
The build directory contains the .tar.gz file and the .sh files from the regular installation package. The two following files have been added manually for the Docker build:

* Dockerfile (required to build the image)
* startup.sh (required to start metrointegrator, ec_postgres, and ec_watchdog at runtime)
 
Here are the detailed instructions how to build the “Base” image and how to create the “Installed” image from there. All docker operations must be executed as root; substitute the MetroIntegrator version number for the appropriate value when installing a different version.

### Build the base image ###


    $ docker build -t albistechnologies/metrointegrator:2.0Build17_Base .


(depending on your hardware, this may take a minute)

### Run the base image ###


    $ docker run -it --net host --name=temp_mi_install albistechnologies/metrointegrator:2.0Build17_Base


This command should execute instantly and put you inside the container. Here you start the installation process:


    $ ./install.sh /opt/ems metrointegrator-2.0-Build17-Linux.tar.gz


Follow the instructions of the installer until the setup has been completed successfully.
Afterwards, it is recommended to wait for the application to start up once (otherwise, you’ll have to wait for the initialization to complete each time you run the installed container, which can take a substantial amount of time depending on your hardware). 
The easiest way is to wait for the login page to be accessible on the MetroIntegrator web server.

After the application has finished initializing, stop all services and exit the container:


    $ service metrointegrator stop
    $ service ec_watchdog stop
    $ service ec_postgres stop
    $ exit


Now, you can commit the container to create the “Installed” image:


    $ docker commit -c "entrypoint /root/MetroIntegrator/startup.sh" temp_mi_install albistechnologies/metrointegrator:2.0Build17_Installed


You can now remove the temporary container:


    $ docker rm temp_mi_install


The image is now ready to be started as a usable container:


    $ docker run -it --net host --rm=true albistechnologies/metrointegrator:2.0Build17_Installed

    
### Note ###
The installation process is not optimally efficient storage-wise since the installation archive (i.e. metrointegrator-2.0-Build17-Linux.tar.gz) is still part of the image, taking up ~600MB of unnecessary disk space.
Deleting the file after the installation is completed won’t help since it is still part of the parent image (i.e. the “Base” image).
This redundancy could be avoided by installing the application automatically during the Docker build and deleting the installation archive before the image gets committed. Alternatively, the .tar.gz file could be copied to the container manually before the installation and erased after it has been extracted.
