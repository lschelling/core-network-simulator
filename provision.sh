#set timezone and keyboard layout for Zurich, Switzerland:
timedatectl set-timezone Europe/Zurich
echo "setxkbmap ch -variant de_sundeadkeys" >> /home/vagrant/.config/lxsession/Lubuntu/autostart

#enable auto-login
echo "autologin-user=vagrant" >> /etc/lightdm/lightdm.conf.d/20-lubuntu.conf
echo "autologin-user-timeout=0" >> /etc/lightdm/lightdm.conf.d/20-lubuntu.conf
echo "greeter-session=lightdm-gtk-greeter" >> /etc/lightdm/lightdm.conf.d/20-lubuntu.conf

#disable screen lock and power manager
mv /etc/xdg/autostart/light-locker.desktop /etc/xdg/autostart/light-locker.desktop.bak
mv /etc/xdg/autostart/xfce4-power-manager.desktop /etc/xdg/autostart/xfce4-power-manager.desktop.bak

apt-get update
apt-get -f
apt-get remove -y update-manager-core
apt-get -y install ssh nano firefox wireshark wireshark-common apt-transport-https vlan iperf libpcap-dev
modprobe 8021q
dpkg-reconfigure wireshark-common -f readline << EOF
Y
EOF
adduser vagrant wireshark
apt-get -y install isc-dhcp-server bridge-utils ebtables telnet traceroute
apt-get -y install libev4
apt-get -y install quagga
wget --quiet http://downloads.pf.itd.nrl.navy.mil/core/packages/4.8/core-daemon_4.8-0ubuntu1_trusty_amd64.deb --no-check-certificate
wget --quiet http://downloads.pf.itd.nrl.navy.mil/core/packages/4.8/core-gui_4.8-0ubuntu1_trusty_all.deb --no-check-certificate
dpkg -i core-daemon_4.8-0ubuntu1_trusty_amd64.deb
dpkg -i core-gui_4.8-0ubuntu1_trusty_all.deb

#add patch for docker V1.8+ compatibility (tested up to Docker V1.9.1)
cp /vagrant/core-patch/dockersvc.py /usr/lib/python2.7/dist-packages/core/services/dockersvc.py
#fix wireshark menu for node numbers greater than 9 (fix hex/dec conflict)
cp /vagrant/core-patch/editor.tcl /usr/lib/core/

update-rc.d core-daemon defaults
service core-daemon start
#echo "dns-nameservers 10.1.0.150 10.1.0.151" >> /etc/network/interfaces
vconfig add eth1 1000
vconfig add eth1.1000 2000

ifdown -a
ifup -a

apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
echo "deb https://apt.dockerproject.org/repo ubuntu-trusty main" > /etc/apt/sources.list.d/docker.list
apt-get update
apt-get -y install linux-image-extra-$(uname -r) docker-engine=1.9.1-0~trusty

echo debconf shared/accepted-oracle-license-v1-1 select true | debconf-set-selections
echo debconf shared/accepted-oracle-license-v1-1 seen true | debconf-set-selections

apt-add-repository ppa:webupd8team/java
apt-get update
apt-get -y install oracle-java8-installer

docker build -t=user/radius /vagrant/docker/radius

mkdir -p /home/vagrant/CORE_Scenarios
cp /vagrant/docker/radius/core-radius.imn /home/vagrant/CORE_Scenarios/core-radius.imn

# install the 802.1ag tools ethping, ethtrace dot1agd dot1ag_ccd from https://github.com/vnrick/dot1ag-utils
cd /vagrant/tools
tar -xzf dot1ag-utils.tgz
cd dot1ag-utils
sh configure
make
make install



